import numpy as np
import cv2
import time
from rpi_ws281x import *
import argparse

# LED strip configuration:
LED_COUNT      = 50      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53
LED_TRANSITION  = 50      # 3 seconds for transition 
FACE_DELAY= 2      # 2 seconds for transition 

ts = int(time.time())

def colorWipe(strip, color, wait_ms=50):
    """Wipe color across display a pixel at a time."""
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, color)
        strip.show()
        time.sleep(wait_ms/1000.0)

def main():
    face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
    cap = cv2.VideoCapture(0)
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
    strip.begin()
    colorWipe(strip,Color(255,255,255),10) 

    while(cap.isOpened()):
        try:
            ret, img = cap.read()
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            faces = face_cascade.detectMultiScale(gray, 1.3, 5)
            height, width, _ = img.shape

#           avg_color_per_row = np.average(img, axis=0)
#           avg_color = np.average(avg_color_per_row, axis=0)   
#           print("R:" + str(avg_color[0]) + "G:" + str(avg_color[1]) + " B:" + str(avg_color[2]))
#           color = Color(int(avg_color[0]/2),int(avg_color[1]/2),int(avg_color[2]/2))
#           colorWipe(strip,color) 
#colorWipe(strip,Color(0,0,0)) 

            for (x,y,w,h) in faces:
                print("found face at x:" + str(x) + " y:" + str(y) + " w:" + str(w) + " h:" + str(h) )
                dress = img[y+h:height, x:x+w]
                colorWipe(strip,Color(255,0,0),10) 
                ts = time.time()
                #avg_color_per_row = np.average(dress, axis=0)
                #avg_color = np.average(avg_color_per_row, axis=0)   
                #color = Color(int(avg_color[0]),int(avg_color[1]),int(avg_color[2]))
                #colorWipe(strip,color) 
            if not faces:
                print("no faces!")
            if ts + FACE_DELAY <= int(time.time()):
                    colorWipe(strip,Color(255,255,255),10) 
            k = cv2.waitKey(30) & 0xff
            if k == 27:
               break

        except IOError:
            print('An error occured trying to read the file.')
        except ValueError:
            print('Non-numeric data found in the file.')
        except ImportError:
            print('NO module found')
        except EOFError:
            print('Why did you do an EOF on me?')
        except KeyboardInterrupt:
            print('You cancelled the operation.')
            exit()
        #except:
            #print('An error occured.')

if __name__ == '__main__':
    main()
