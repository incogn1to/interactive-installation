#!/usr/bin/env python

import RPi.GPIO as GPIO
import time
import picamera
import sys

GPIO.setwarnings(False)

step_pin = 24
dir_pin = 22
opto_pin = 17
servo_pin = 26


GPIO.setmode(GPIO.BCM)
GPIO.setup(step_pin, GPIO.OUT)
GPIO.setup(dir_pin, GPIO.OUT)
GPIO.setup(opto_pin, GPIO.IN)
GPIO.setup(servo_pin, GPIO.OUT)
GPIO.output(dir_pin, GPIO.LOW)

try:
    camera = picamera.PiCamera()
    camera.capture('camera.jpg')  
    
except KeyboardInterrupt:
    GPIO.cleanup()
except:
    print("Unexpected error:", sys.exc_info()[0])
    raise

