import numpy as np
import cv2
import time
import argparse
import random
import serial
import sys
from rpi_ws281x import *

# LED strip configuration:
#LED_COUNT      = 50      # Number of LED pixels.
LED_COUNT      = 46      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53
LED_TRANSITION  = 50      # 3 seconds for transition 
MIN_MOV  = 50      
FACE_DELAY= 3      # 2 seconds for transition 

THRESHOLD = 600
FALSE_POSITIVE = 20000
PIXEL_INTENSITY_THRESHOLD = 20

face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)

def startUVLeds():
    return setMotor(1,1,100)

def stopUVLeds():
    return setMotor(1,1,0)

def startMotor():
    setMotor(0,getRotationDirection(),50)

def stopMotor():
    setMotor(0,1,0)

def startSmallEngines():
    setMotor(2,getRotationDirection(),50)
    setMotor(3,getRotationDirection(),50)
    setMotor(4,getRotationDirection(),50)

def stopSmallEngines():
    setMotor(2,0,0)
    setMotor(3,0,0)
    setMotor(4,0,0)

def getRotationDirection():
    return random.randint(0,1) 

def HSVBin(img):
    hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)
    lower_skin = np.array([50, 45, 0])
    upper_skin = np.array([125, 125, 255])
    mask = cv2.inRange(hsv, lower_skin, upper_skin)
    return mask

def boundaries(number, mmin, mmax):
    scaled = int(number / mmax)
    if scaled < mmin:
        scaled = mmin
    if scaled > mmax:
        scaled = mmax
    return scaled

def control_bytes(num, dire, spd):
    byte_array = [num | dire << 4, spd]
    return bytearray("".join(chr(val) for val in byte_array), encoding='ascii')

def setMotor(motor,direction,speed):
    #print("set motor to")
    with serial.Serial('/dev/ttyACM0', 115200) as ser:
        ser.write(control_bytes(motor, direction, speed))

def detectMovement(fgbg,kernel,diffImg):
    movement = 0
    threshImg = cv2.threshold(diffImg, PIXEL_INTENSITY_THRESHOLD, 255, cv2.THRESH_BINARY)[1]
    gray = cv2.cvtColor(threshImg, cv2.COLOR_BGR2GRAY)
    fgmask = fgbg.apply(gray)
    fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, kernel)
    whitePixels = cv2.findNonZero(fgmask)
    #print("whitepixels:"+str(whitePixels))
    if whitePixels is not None:
        amountOfMovement = len(whitePixels)
        #print("amountOfMovement:"+str(amountOfMovement))
        #if (amountOfMovement > THRESHOLD and amountOfMovement < FALSE_POSITIVE):
        movement = boundaries(amountOfMovement, 0, 100)
    #print("movement:"+str(movement))
    return movement

def setIdle():
    print("set to idle...")
    colorWipe(strip,Color(50,50,50),1) # set to white color
    stopSmallEngines()
    stopUVLeds()
    stopMotor()

def getContours(img):
    kernel = np.ones((5,5),np.uint8)
    closed = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)
    closed = cv2.morphologyEx(closed, cv2.MORPH_CLOSE, kernel)
    img, contours, h  = cv2.findContours(closed, cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    validContours = [];
    for cont in contours:
        if cv2.contourArea(cont) > 9000:
            # x,y,w,h = cv2.boundingRect(cont)
            # if h/w > 0.75:
            validContours.append(cv2.convexHull(cont))
            # rect = cv2.minAreaRect(cont)
            # box = cv2.cv.BoxPoints(rect)
            # validContours.append(np.int0(box))
    return validContours

def getAveragePixel2(img):
    data = np.reshape(img, (-1,3))
    data = np.float32(data)
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 1.0)
    flags = cv2.KMEANS_RANDOM_CENTERS
    compactness,labels,centers = cv2.kmeans(data,1,None,criteria,10,flags)
    result = centers[0].astype(np.int32) 
    print("avg color R:" + str(result[2]) + " G:" + str(result[1]) + " B:" + str(result[0]))
    return Color(int(result[2]),int(result[1]),int(result[0]))

def getAveragePixel(img):
    avg_color_per_row = np.average(img, axis=0)
    result = np.average(avg_color_per_row, axis=0)   
    color = Color(int(result[2]),int(result[1]),int(result[0]))
    # BGR
    print("avg color R:" + str(int(result[2])) + " G:" + str(int(result[1])) + " B:" + str(int(result[0])))
    return color 

def getFaces(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    return face_cascade.detectMultiScale(gray, 1.3, 5)

def colorWipe(strip, color, wait_ms=50):
    """Wipe color across display a pixel at a time."""
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, color)
        strip.show()
        #time.sleep(wait_ms/1000.0)

def adjustedColor(avg_color):
    print("unadjusted color R:" + str(avg_color[0]) + " G:" + str(avg_color[1]) + " B:" + str(avg_color[2]))
    avg_color[0]=avg_color[0]+50
    avg_color[1]=avg_color[1]+50
    #avg_color[2]=avg_color[2]+50
    #if avg_color[0] > 255:
    #    avg_color[0] = 255
    #avg_color[0]=avg_color[0]*avg_color[0]
    #if avg_color[0] >= avg_color[1]:
    #    avg_color[0]=255
    #    avg_color[1]=0
    #    avg_color[2]=0
    #elif avg_color[1] >= avg_color[0]:
    #    avg_color[1]=255
    #    avg_color[0]=0
    #    avg_color[2]=0
    #elif avg_color[2] >= avg_color[0] and avg_color[2] >= avg_color[1]:
    #    avg_color[2]=255
    print("adjusted color R:" + str(avg_color[0]) + " G:" + str(avg_color[1]) + " B:" + str(avg_color[2]))
    return Color(int(avg_color[0]),int(avg_color[1]),int(avg_color[2]))

def main():
    ts = int(time.time())
    cap = cv2.VideoCapture(0)
    strip.begin()
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
    fgbg = cv2.createBackgroundSubtractorMOG2()
    setIdle()
    ret,img0 = cap.read()

    while(cap.isOpened()):
        try:
            ret, img1 = cap.read()
            height, width, channels = img1.shape
            faces = getFaces(img1)

            for (x,y,w,h) in faces:
                print("face at x:" + str(x) + " y:" + str(y) + " w:" + str(w) + " h:" + str(h) )
                dress = img1[y+h:height, x:x+w]
                avg_color = getAveragePixel2(dress)
                colorWipe(strip,avg_color,10) 
                startMotor()
                startUVLeds()
                ts = time.time()
            if len(faces)==0:
                if ts + FACE_DELAY <= int(time.time()):
                    print("no faces!")
                    setIdle()

            diffImg = cv2.absdiff(img0,img1)
            mov = detectMovement(fgbg,kernel,diffImg)
            if mov > MIN_MOV:
                print('movement detected:'+str(mov))
                startSmallEngines()

            img0 = img1
            k = cv2.waitKey(30) & 0xff
            if k == 27:
               break

        except IOError:
            print('An error occured trying to read the file.')
        except ValueError:
            print('Non-numeric data found in the file.')
        except ImportError:
            print('NO module found')
        except EOFError:
            print('Why did you do an EOF on me?')
        except KeyboardInterrupt:
            print('You cancelled the operation.')
            setIdle()
            stopSmallEngines()
            exit()
        #except:
            #print('An error occured.')

    setIdle()
    stopSmallEngines()

if __name__ == '__main__':
    main()
