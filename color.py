import cv2
import numpy as np

# Capture the input frame from webcam
def get_frame(cap, scaling_factor):
    # Capture the frame from video capture object
    ret, frame = cap.read()

    # Resize the input frame
    frame = cv2.resize(frame, None, fx=scaling_factor,
            fy=scaling_factor, interpolation=cv2.INTER_AREA)

    return frame

if __name__=='__main__':
    cap = cv2.VideoCapture(0)
    scaling_factor = 0.5

    # Iterate until the user presses ESC key
    while True:
        frame = get_frame(cap, scaling_factor)

        # Convert the HSV colorspace
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        boundaries = [
                ([17, 15, 100], [50, 56, 200]),
                ([86, 31, 4], [220, 88, 50]),
                ([25, 146, 190], [62, 174, 250]),
                ([103, 86, 65], [145, 133, 128])
        ]

        # 'blue' range in HSV colorspace
        blue_lower = np.array([100,150,0])
        blue_upper = np.array([140,255,255])
        red_lower = np.array([0,100,100])
        red_upper = np.array([10,255,255])
        green_upper = np.array([60,100,100])
        green_lower = np.array([60,255,255])

        # Threshold the HSV image to get only blue color
        blue_mask = cv2.inRange(hsv, blue_lower, blue_upper)
        red_mask = cv2.inRange(hsv, red_lower, red_upper)
        green_mask = cv2.inRange(hsv, green_lower, green_upper)

        # Bitwise-AND mask and original image
        res = cv2.bitwise_and(frame, frame, mask = blue_mask|red_mask|green_mask)
        res = cv2.medianBlur(res, 5)

        cv2.imshow('Original image', frame)
        cv2.imshow('Color Detector', res)

        # Check if the user pressed ESC key
        c = cv2.waitKey(5)
        if c == 27:
            break

    cv2.destroyAllWindows()
